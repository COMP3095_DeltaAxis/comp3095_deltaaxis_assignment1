/*********************************************************************************
* Project: COMP3095_DeltaAxis
* Assignment: Assignment 1
* Author(s): Jacky, Jullian, Jarone, Charles
* Student Number: 100801047, 100998164, 101077225, 101084441
* Date: Oct 28, 2018
* Description: Servlet that handles user registration and accesses the DB
	to insert new user data if info is valid.
*********************************************************************************/
package reg;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import helper.*;
import utilities.*;

/**
 * Servlet implementation class Registration
 */
@WebServlet("/Registration")
public class Registration extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Registration() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String register = request.getParameter("register");

		if (register.toLowerCase().equals("cancel"))
			response.sendRedirect("login.jsp");	
		else if (register.toLowerCase().equals("register")) {
			String firstname = request.getParameter("firstname");
			String lastname = request.getParameter("lastname");
			String email = request.getParameter("email");
			String password = request.getParameter("password");
			String confirmPassword = request.getParameter("confirm-password");
			String agree = request.getParameter("agree");
			
			boolean isValid = true;

			// Check for missing values
			isValid = !Helper.isMissing(firstname);
			isValid = isValid ? !Helper.isMissing(firstname) : false;
			isValid = isValid ? !Helper.isMissing(lastname) : false;
			isValid = isValid ? !Helper.isMissing(email) : false;
			isValid = isValid ? !Helper.isMissing(password) : false;
			isValid = isValid ? !Helper.isMissing(confirmPassword) : false;
			isValid = isValid ? !Helper.isMissing(agree) : false;

			// Validate each form value
			isValid = isValid ? (Helper.isAlphabets(firstname) && Helper.isAlphabets(lastname)) : false;
			isValid = isValid ? Helper.length(password) : false;
			isValid = isValid ? Helper.isEmail(email) : false;
			isValid = isValid ? Helper.hasSpecialChar(password) : false;
			isValid = isValid ? Helper.hasUpperCase(password) : false;
			isValid = isValid ? password.equals(confirmPassword) : false;

			if(isValid) {
				try {
					Connection con = utilities.DatabaseAccess.connectDataBase();
					String userQuery = String.format("SELECT * FROM USERS WHERE email = '%s'", email);
					ResultSet userSet = con.prepareStatement(userQuery).executeQuery();
					userSet.next();
					if(userSet.getRow()==1) {
						response.sendRedirect("invalidregistration.jsp");
					}
					else {
						String insertUser = String.format("INSERT INTO USERS (firstname, lastname, email, password) VALUES ('%1$s', '%2$s', '%3$s', '%4$s')", firstname, lastname, email, password);
						con.prepareStatement(insertUser).executeUpdate();
						String insertUserRole = String.format("INSERT INTO USER_ROLES() VALUES ((SELECT id FROM USERS WHERE email = '%1$s'), (SELECT roleid FROM ROLES WHERE rolename='user'))", email);
						con.prepareStatement(insertUserRole).executeUpdate();
						response.sendRedirect("success.jsp");
					}
				}
				catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else
				response.sendRedirect("invalidregistration.jsp");
		}
		else
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
	}
}
