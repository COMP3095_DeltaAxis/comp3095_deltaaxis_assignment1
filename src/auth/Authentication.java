/*********************************************************************************
* Project: COMP3095_DeltaAxis
* Assignment: Assignment 1
* Author(s): Jacky, Jullian, Jarone, Charles
* Student Number: 100801047, 100998164, 101077225, 101084441
* Date: Oct 28, 2018
* Description: Servlet that handles authentication and accesses the DB
*********************************************************************************/
package auth;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import helper.Helper;
import utilities.*;

/**
 * Servlet implementation class Authentication
 */
@WebServlet("/Authentication")
public class Authentication extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Authentication() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		String action = request.getParameter("action");
		if (action.toLowerCase().equals("logout")) {
			session.invalidate();
			request.getRequestDispatcher("login.jsp").forward(request, response);
		} 
		else {
			response.setContentType("application/json");
			response.getWriter().write("{ \"message\" : \"action is not valid\" }");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		if (!Helper.isMissing(username) && !Helper.isMissing(password)) {
			try {
				Connection con = utilities.DatabaseAccess.connectDataBase();
				String usernameCol = "email";
				String passwordCol = "password";
				String userQuery = String.format("SELECT * FROM USERS WHERE email = '%s'", username);
				ResultSet userSet = con.prepareStatement(userQuery).executeQuery();
				userSet.next();
				
				if (userSet.getRow() == 1) {
					User user = new User();
					user.id(userSet.getInt("id"));
					user.firstname(userSet.getString("firstname"));
					user.lastname(userSet.getString("lastname"));
					user.email(userSet.getString("email"));
					user.password(userSet.getString("password"));
					
					String roleQuery = String.format("SELECT rolename FROM ROLES WHERE roleid = (SELECT roleid FROM USER_ROLES WHERE userid = %d)", user.id());
					ResultSet roleSet = con.prepareStatement(roleQuery).executeQuery();
					roleSet.next();
					
					if (roleSet.getRow() == 1) {
						user.role(roleSet.getString("rolename"));
						if (username.equals(user.email()) && password.equals(user.password())) {
							session.setAttribute("user", user);
							response.sendRedirect("dashboard.jsp");
						}
					}
					else response.sendRedirect("loginerror.jsp");
					
					con.close();
				}
			} catch (Exception e) {
				response.sendRedirect("loginerror.jsp");
			}
		}
		else response.sendRedirect("loginerror.jsp");
	}

}
