/*********************************************************************************
* Project: COMP3095_DeltaAxis
* Assignment: Assignment 1
* Author(s): Jacky, Jullian, Jarone, Charles
* Student Number: 100801047, 100998164, 101077225, 101084441
* Date: Oct 28, 2018
* Description: Class that has useful methods for handling validation 
  & other tasks
*********************************************************************************/
package helper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Helper {
	public static boolean isMissing(String s) {
		return s == null || s.isEmpty() || s.trim().equals("");
	}
	
	public static boolean isAlphabets(String name) {
		return name.matches("[a-zA-Z]{1}[a-zA-Z -]*[a-zA-Z]{1}");
	}
	
	public static boolean length(String password) {
		return ((password.length() >= 6) && password.length() <= 12);
	}
	
	public static boolean hasSpecialChar(String password) {
		Pattern pattern = Pattern.compile("[!@#$%&*()_+=|<>?{}\\[\\]~-]");
		Matcher match = pattern.matcher(password);
		return match.find();
	}
	
	public static boolean hasUpperCase(String password) {
		for(int i = 0; i < password.length(); i++) {
			char c = password.charAt(i);
			if(Character.isUpperCase(c))
				return true;
		}
		return false;
	}
	
	public static boolean isEmail(String email) {
		Pattern pattern = Pattern.compile("^([\\w-\\.]+){1,64}@([\\w&&[^_]]+){2,255}[.][a-z]{2,}$");
		Matcher match = pattern.matcher(email);
		return match.matches();
	}
}
