/*********************************************************************************
* Project: COMP3095_DeltaAxis
* Assignment: Assignment 1
* Author(s): Jacky, Jullian, Jarone, Charles
* Student Number: 100801047, 100998164, 101077225, 101084441
* Date: Oct 28, 2018
* Description: Object Model of Site Users
*********************************************************************************/
package utilities;

public class User {
	private Integer id = null;
	private String firstname = null;
	private String lastname = null;
	private String email = null;
	private String password = null;
	private String role = null;
	
	public User() { }
	
	public User(Integer id, String firstname, String lastname, String email, String password, String role) {
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.password = password;
		this.role = role;
	}
	
	public Integer id() { return id; }
	public void id(Integer id) { this.id = id; }
	
	public String firstname() { return firstname; }
	public void firstname(String firstname) { this.firstname = firstname; }
	
	public String lastname() { return firstname; }
	public void lastname(String lastname) { this.lastname = lastname; }
	
	public String email() { return email; }
	public void email(String email) { this.email = email; }
	
	public String password() { return password; }
	public void password(String password) { this.password = password; }
	
	public String role() { return role; }
	public void role(String role) { this.role = role; }
}
