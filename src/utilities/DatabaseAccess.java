/*********************************************************************************
* Project: COMP3095_DeltaAxis
* Assignment: Assignment 1
* Author(s): Jacky, Jullian, Jarone, Charles
* Student Number: 100801047, 100998164, 101077225, 101084441
* Date: Oct 28, 2018
* Description: Class that connects to the database of the web server
*********************************************************************************/
package utilities;
/****************************************************************************************************
* Description: DatabaseAccess - Example provides access to database
****************************************************************************************************/
import java.sql.Connection;
import java.sql.DriverManager;

public class DatabaseAccess {
	private static String username = "admin@domain.ca";;
	private static String password = "P@ssword1";
	private static String database = "COMP3095";
	private static Connection connect = null;
  
	public static Connection connectDataBase() throws Exception {
		try {
			// This will load the MySQL driver, each DB has its own driver
			Class.forName("com.mysql.jdbc.Driver");
			// Setup the connection with the DB
			connect = DriverManager
					.getConnection("jdbc:mysql://localhost:3306/" + database + "?"
							+ "user=" + username + "&password=" + password);
			return connect;
		} catch (Exception e) {
			throw e;
		} 
	}
}