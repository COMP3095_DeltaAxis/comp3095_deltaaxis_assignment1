/*********************************************************************************
* Project: COMP3095_DeltaAxis
* Assignment: Assignment 1
* Author(s): Jacky, Jullian, Jarone, Charles
* Student Number: 100801047, 100998164, 101077225, 101084441
* Date: Oct 28, 2018
* Description: Servlet that handles login info validation and sends
	info to authentication servlet for processing.
*********************************************************************************/

package login;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import helper.*;
import utilities.*;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String login = request.getParameter("login");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String grecaptchaResponse = request.getParameter("g-recaptcha-response");
		boolean isValid = true;

		if(login.toLowerCase().equals("registration"))
			response.sendRedirect("registration.jsp");
		else if (login.toLowerCase().equals("login")) {
			isValid = isValid ? Helper.isEmail(username) : false;
			isValid = isValid ? !Helper.isMissing(grecaptchaResponse) : false;
			if(isValid) {
				request.setAttribute("uesrname", username);
				request.setAttribute("password", password);
				request.getRequestDispatcher("Authentication").forward(request, response);
			}
			else
				response.sendRedirect("loginerror.jsp");
		}
		else 
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
	}
}
