DROP DATABASE IF EXISTS COMP3095;

CREATE DATABASE IF NOT EXISTS COMP3095;
USE COMP3095;
grant all on COMP3095.* to 'admin@domain.ca'@'localhost' identified by 'P@ssword1'; 

CREATE TABLE USERS 
( 
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT, 
    firstname VARCHAR(255),
    lastname VARCHAR(255),
    email VARCHAR(255), 
    created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    password VARCHAR(20)    
);

CREATE TABLE ROLES
(
    roleid INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    rolename VARCHAR(20)
);

CREATE TABLE USER_ROLES
(
    userid INT(11),
    roleid INT(11),
CONSTRAINT user_roles_pk PRIMARY KEY (userid, roleid),
CONSTRAINT user_id_fk FOREIGN KEY (userid) REFERENCES USERS (id),
CONSTRAINT user_role_fk FOREIGN KEY (roleid) REFERENCES ROLES (roleid)
);
 

INSERT INTO ROLES (rolename)
VALUES ('admin');

INSERT INTO ROLES (rolename)
VALUES ('user');

INSERT INTO USERS (firstname, lastname, email, password) 
VALUES ('admin', '', 'admin@isp.net', 'P@ssword1');

INSERT INTO USER_ROLES ()
VALUES ((SELECT id FROM USERS WHERE email = 'admin@isp.net'), (SELECT roleid FROM ROLES WHERE rolename = 'admin'));