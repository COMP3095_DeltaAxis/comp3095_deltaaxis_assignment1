<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>COMP3095 Assignment 1</title>
<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/css/styles.css"/>
</head>
<body>
	<div class="content-body">
		<div class="navbar-body"></div>
		<form class="registration" name="user-registration" action="register" method="POST">
				<div id="message" class="alert alert-danger" role="alert">
					<ul class="m-0">
						<li>The password must contain one uppercase and special character, and must be between 6-12 characters.</li>
						<li>You must enter a valid email.</li>
						<li>All fields must be filled out.</li>
					</ul>
				</div>
				<h1 class="no-select mb-3">Registration</h1>
				<div class="input-group mb-3">
					<div class="input-group-prepend">
						<span class="input-group-text no-select" id="first-name-label">First Name</span>
					</div>
					<input type="text" id="firstname" name="firstname" 
						class="form-control" aria-label="firstname" aria-describedby="firstname" />
				</div>
				<div class="input-group mb-3">
					<div class="input-group-prepend">
						<span class="input-group-text no-select" id="last-name-label">Last Name</span>
					</div>
					<input type="text" id="last-name" name="lastname" 
						class="form-control" aria-label="lastname" aria-describedby="lastname" />
				</div>
				<div class="input-group mb-3">
					<div class="input-group-prepend">
						<span class="input-group-text no-select" id="email-label">Email</span>
					</div>
					<input type="text" id="email" name="email" 
						class="form-control" aria-label="email" aria-describedby="email" />
				</div>
				<div class="input-group mb-3">
					<div class="input-group-prepend">
						<span class="input-group-text no-select" id="password-label">Password</span>
					</div>
					<input type="password" id="password" name="password" 
						class="form-control" aria-label="password" aria-describedby="password" />
				</div>
				<div class="input-group mb-3">
					<div class="input-group-prepend">
						<span class="input-group-text no-select" id="confirm-password-label">Confirm Password</span>
					</div>
					<input type="password" id="confirm-password" name="confirm-password" 
						class="form-control" aria-label="confirm-password" aria-describedby="confirm-password" />
				</div>
				<div class="text-center mb-3">
					<input type="checkbox" id="agree" name="agree" value="agree" aria-label="agree" aria-describedby="agree"> 
					<label for="agree" class="no-select" style="color: white !important;">I agree to the Terms of Services</label>
				</div>
				<div id="form-btns" class="text-right">
					<button type="submit" id="reg-cancel-btn" class="btn btn-secondary" name="register" value="cancel">Cancel</button>
					<button type="submit" id="registration-btn" class="btn btn-primary" name="register" value="register">Register</button>
				</div>
			</form>
		</div>
</body>
</html>