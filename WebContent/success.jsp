<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>COMP3095 Assignment 1</title>
<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/css/styles.css"/>
</head>
<body>
  <div class="content-body">
    <div class="navbar-body"></div>
    <div class="fixed-center" id="registration-message">
      <h1>You have successfully registered!</h1>
      <h3>An email has been sent to your inbox.</h3>
      <h4><a href="./">Return to Login</a></h4>
    <div>
  </div>
</body>
</html>