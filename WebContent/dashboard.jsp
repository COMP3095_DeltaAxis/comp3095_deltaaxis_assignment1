<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>COMP3095 Assignment 1</title>
<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/css/styles.css"/>
</head>
<body>
  <div class="content-body">
    <div class="navbar-body">
      <div class="navbar-inner">
        <div class="navbar-options left">
          <a class="tab no-select" href="#">Tab 1</a>
          <a class="tab no-select" href="#">Tab 2</a>
          <a class="tab no-select" href="#">Tab 3</a>
          <a class="tab no-select" href="#">Tab 4</a>
        </div>
        <div class="navbar-options right">
          <form action="auth" method="GET"><button class="btn btn-primary" type="submit" name="action" value="logout">Log Out</button></form>
        </div>
      </div>
    </div>
    <div class="content-area">
      <div class="content-block">
        <h1 class="text-center">Future Enhancements</h1>
        <img id="under-construction" src="assets/images/underconstruction.png" alt="Under Construction" />
      </div> 
    </div>
  </div>
</body>
</html>