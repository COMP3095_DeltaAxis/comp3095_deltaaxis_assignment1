<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>COMP3095 Assignment 1</title>
<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/css/styles.css"/>
</head>
<body>
  <div class="content-body">
    <div class="navbar-body"></div>
    <div class="fixed-center text-center" id="access-denied-message">
      <h1>You must be logged in to access this page.</h1>
      <h4><a href="./">Click here to Login</a></h4>
    </div>
  </div>
</body>
</html>