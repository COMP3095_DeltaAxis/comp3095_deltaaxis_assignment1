<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>COMP3095 Assignment 1</title>
<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="assets/css/styles.css"/>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
</head>
<body>
	<div class="content-bpdy">
		<div class="navbar-body"></div>
		<form class="login" name="user-login" action="login" method="POST">
			<h1 class="no-select mb-3">Login</h1>
			<div class="input-group mb-3">
				<div class="input-group-prepend">
					<span class="input-group-text no-select" id="username-label">Username</span>
				</div>
				<input type="text" id="username" name="username" class="form-control" aria-label="username" aria-describedby="username">
			</div>
			<div class="input-group mb-3">
				<div class="input-group-prepend">
					<span class="input-group-text no-select" id="password-label">Password</span>
				</div>
				<input type="password" id="password" name="password" class="form-control" aria-label="password" aria-describedby="password">
			</div>
			<div class="g-recaptcha" data-sitekey="6Ldp7nYUAAAAAKw_QtvDBVI1h6coZ5jEMXIVh89m"></div>
			<br>
			<div id="form-btns" class="text-right">
				<button type="submit" id="registration-btn" class="btn btn-secondary" name="login" value="registration">Register</button>
				<button type="submit" id="login-btn" class="btn btn-primary" name="login" value="login">Login</button>
			</div>
		</form>
	</div>
</body>
</html>